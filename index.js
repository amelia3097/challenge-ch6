const { render } = require("ejs")
const express = require("express")
const app = express()
const users = require("./api/users")
const path = require('path')

app.use(express.static(path.join(__dirname,'./public')))

app.set("view engine", "ejs")

app.use("/api/v1", users)

app.use(express.json())

app.use(express.urlencoded({extended: true}))

let alreadylogin = false

validate = (req, res, next) => {
    if (alreadylogin == false) {
        res.redirect('/dashboard/login')
    } 
    else {
        next()
    }
}

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname+'/public/index.html'))
})

app.get('/game', (req, res) => {
    res.sendFile(path.join(__dirname+'/public/chpter4.html'))
})

app.get('/dashboard/login', (req, res) => {
    res.render("login")
})

app.post("/dashboard/login", (req, res) => {
    if (req.body.username == 'admin' && req.body.password == 'admin') {
        alreadylogin = true
        res.redirect('/dashboard/')
    }
    else {
        alreadylogin = false
        res.redirect('/dashboard/login')}
})

app.get("/dashboard/logout", (req, res) => {
    alreadylogin = false
    res.redirect('/dashboard/login')
})

// List view
app.get("/dashboard", validate, (req, res) => {

    res.render("users/list")
})

// form create
app.get("/dashboard/users/form", validate, (req, res) => {
    res.render("users/form", { id: null })
})

// form edit
app.get("/dashboard/users/form/:id", validate, (req, res) => {
    res.render("users/form", { id: req.params.id })
})

// Detail view
app.get("/dashboard/users/:id", validate, (req, res) => {
    res.render("users/detail", {
        id: req.params.id
    })
})

app.listen(8000, () => console.log(`Server running on port 8000`))