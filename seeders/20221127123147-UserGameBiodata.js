'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('UserGameBiodata', [{
      id: 1,
      firstname: "admin",
      lastname: "admin",
      createdAt: new Date(),
      updatedAt: new Date()
    } , {
     id: 2,
     firstname: "sample",
     lastname: "sample",
     createdAt: new Date(),
     updatedAt: new Date()
   }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('UserGameBiodata', null, {id: [1, 2]});
  }

};
