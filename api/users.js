const express = require("express")
const router = express.Router()
const cors = require("cors")
const { UserGame, UserGameBiodata, UserGameHistory } = require("../models")

router.use(cors())
router.use(express.json())

// List all articles
router.get("/usergame", async (req, res) => {
    let response = []
    try {
        response = await UserGame.findAll({
            include: [
                {
                    model: UserGameBiodata,
                    as: "usergamebiodata",
                    // required: true
                }
            ]
        })
    } catch (e) {
        console.log(e.message)
    }

    res.json(response)
})

// Get single article by id
router.get("/usergame/:id", async (req, res) => {
    let response = {}
    try {
        response = await UserGame.findOne({
            where: {id: req.params.id},
            include: [
                {
                    model: UserGameBiodata,
                    as: "usergamebiodata",
                    // required: true
                }
            ]

        })
    } catch (e) {
        console.log(e.message)
        response.error = e.message
        res.status(401)
    }

    res.json(response)
})

// Create new article
router.post("/usergame", async (req, res) => {
    try {
        
       let a = await UserGame.create({
            username: req.body.username,
            password: req.body.password,
            // createdAt: new Date(),
            // updatedAt: new Date(),
        })
        await UserGameBiodata.create({
            id:a.id,
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            // createdAt: new Date(),
            // updatedAt: new Date(),
        })
    } catch (e) {
        console.log(e)
        console.log(e.message)
    }

    res.end()
})

// Update article by id
router.put("/usergame/:id", async (req, res) => {
    try {
            await UserGame.update({
            username: req.body.username,
            password: req.body.password,
            
        }, {
            where: {id: req.params.id}

        })

        await UserGameBiodata.update({
            firstname: req.body.firstname,
            lastname: req.body.lastname,
        }, {
            where: {id: req.params.id}

        })
        
    } catch (e) {
        console.log(e.message)
    }

    res.end()
})

// Remove article by id
router.delete("/usergame/:id", async (req, res) => {
    try {
        await UserGame.destroy({
            where: {id: req.params.id}
        })
        await UserGameBiodata.destroy({
            where: {id: req.params.id}
        })
    } catch (e) {
        console.log(e.message)
    }

    res.end()
})

router.get("/usergamebiodata", async (req, res) => {
    let response = []

    try {
        response = await UserGameBiodata.findAll()
    } catch (e) {
        console.log(e.message)
    }

    res.json(response)
})

module.exports = router